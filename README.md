# Summary

These tools are to help serve offensive security professionals in Exploit Development.

We sincerely hope this helps you guys! 
    - Chef

**Hyperion Gray Resources on Windows Exploit Development**



***Resources***



[Geoff Chappell ](https://www.geoffchappell.com/studies/windows/km/index.htm)


[ReactOS](https://www.geoffchappell.com/studies/windows/km/index.htm)


[Virgilis Project](https://www.vergiliusproject.com/)



[ZOF Course](https://www.vergiliusproject.com/)


[GunGage Course](https://www.vergiliusproject.com/)


[BlueBerry Google Sheet](https://www.vergiliusproject.com/)


[VX Underground](https://www.vergiliusproject.com/)


[Shogun Lab](https://www.shogunlab.com/blog/)


[Corelan](https://www.shogunlab.com/blog/)


[Ezpqlusia Blog](https://ezqelusia.blogspot.com)


[Secalert Blog ](https://secalert.net)


[Redplaits Blog](https://redplait.blogspot.com)


[Dimitry Fourny Blog](https://dimitrifourny.github.io)


[Unknown Cheats](https://unknowncheats.me)


[Research Drivers](https://www.unknowncheats.me/forum/anti-cheat-bypass/334557-vulnerable-driver-megathread.html)





***Papers***

[Enumerating RWX Protected Memory Regions](https://ired.team/offensive-security/defense-evasion/finding-all-rwx-protected-memory-regions)


[x86 Guide](https://www.cs.virginia.edu/~evans/cs216/guides/x86.html)


[C Function Call Conventions and The Stack](https://www.csee.umbc.edu/~chang/cs313.s02/stack.shtml)


[Secrets of Reverse Engineering](https://www.foo.be/cours/dess-20122013/b/Eldad_Eilam-Reversing__Secrets_of_Reverse_Engineering-Wiley(2005).pdf)


[x86 API Hooking](http://jbremer.org/x86-api-hooking-demystified/)


[x86 Trampoline Hooking](https://reverseengineering.stackexchange.com/questions/17995/asm-code-hook-patching-with-trampoline-function)


[Windows System Calls](https://github.com/j00ru/windows-syscalls)


[Driver Signing Bypass](http://www.sekoia.fr/blog/windows-driver-signing-bypass-by-derusbi/)


[What Is SMEP?](https://j00ru.vexillium.org/2011/06/smep-what-is-it-and-how-to-beat-it-on-windows/)


[SMEP Bypass](https://connormcgarr.github.io/x64-Kernel-Shellcode-Revisited-and-SMEP-Bypass/)


[Polymoprhic Shellcode Development](https://rastating.github.io/creating-polymorphic-shellcode/)





***Tools***
__________________________________________________________________________________________________________________________________________________________________________________

**Disassemblers, Debuggers, Static and Dynamic Analysis, Reverse Engineering Tools**

[OllyDBG](http://www.ollydbg.de/)

[VivienneVMM](https://github.com/changeofpace/VivienneVMM)


[Immunity Debugger](http://debugger.immunityinc.com/)


[WinDBG](https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/debugger-download-tools) 


[x64 Dbg ](https://x64dbg.com/#start)


[Evans Debugger](http://codef00.com/projects#debugger)


[ExeInfo](http://exeinfo.byethost18.com/?i=1)


[Scylla](https://github.com/NtQuery/Scylla)


[PEStudio ](https://winitor.com/)


[SysInternals](https://docs.microsoft.com/en-us/sysinternals/)


[NirSoft Tools](https://www.nirsoft.net/)


[Process Hacker](https://processhacker.sourceforge.io/)


[OSRDriverLoader](https://www.osronline.com/article.cfm%5Earticle=157.htm)


[Mona](https://github.com/corelan/mona)


[IDAPro](https://www.hex-rays.com/products/ida/)


[API Monitor](http://www.rohitab.com/apimonitor)


[DNSpy](https://github.com/0xd4d/dnSpy)


[Snowman](https://derevenets.com/)


[RetSync](https://github.com/bootleg/ret-sync)


[Flare](https://github.com/fireeye/flare-kscldr)


[Pinjectra](https://github.com/SafeBreach-Labs/pinjectra)


[PolyChaos](https://github.com/DarthTon/Polychaos)


[UEFI Tool](https://github.com/LongSoft/UEFITool)


[HideDriver](https://github.com/szdyg/HideDriver)


[WinRing0](https://github.com/kagasu/WinRing0)


[Common Anti-Debugging](https://github.com/ThomasThelen/AntiDebugging)


[PolyMutex](https://github.com/odzhan/polymutex)


[PolyHook](https://github.com/haidragon/PolyHook_2_0)


[Little C Compiler](https://github.com/751643992/LittleCCompiler)
